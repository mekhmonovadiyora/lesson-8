
const appendSeconds = document.getElementById('seconds')
const appendTens = document.getElementById('tens')
const time = document.getElementById('time')

const add = document.getElementsByClassName('add')[0]
const startPause = document.getElementsByClassName('start-pause')[0]
const reset = document.getElementsByClassName('reset')[0]

let sec, tens, interval

// paused time after refreshing the page

if (localStorage.getItem("pausedTime")) {
	    pausedTime = JSON.parse(localStorage.getItem("pausedTime"))
	    sec = pausedTime[0]
	    tens = pausedTime[1]
		secStr = (sec.toString().length == 1) ? `0${sec}`: sec
    	tensStr = (tens.toString().length == 1) ? `0${tens}` : tens
		appendSeconds.innerText = secStr
		appendTens.innerText = tensStr
	    console.log(secStr, tensStr)
} else {
		sec = 00
		tens = 00
}

// saved time after refreshing the page

if (localStorage.getItem("last-time")) {
    displayTime = JSON.parse(window.localStorage.getItem('last-time'))
    
    displayTime.forEach(element => {
        const div = document.createElement('div');
        div.className = 'add_time';
        div.innerText = element
        document.querySelector('section').appendChild(div);

        const button = document.createElement('button');
        button.className = 'add_time_button';
        button.innerText = 'X'
        button.addEventListener('click', function() {
            document.querySelector('section').removeChild(button);
            document.querySelector('section').removeChild(div);
        })
        document.querySelector('section').appendChild(button);
    });
}

// change start-pause button

function changeLabel() {

	if ( startPause.innerText == 'Start' ) {
		startPause.innerText = 'Pause'
		interval = setInterval(startTimer, 10)

	} else {
		startPause.innerText = 'Start'
		const lastPausedSec = sec
        const lastPausedTens = tens
        window.localStorage.setItem('pausedTime', JSON.stringify([lastPausedSec, lastPausedTens]))
		clearInterval(interval)
	}

}

// number formatting

function startTimer () {
    tens++ 
    if ( tens <= 9 ) {
    	appendTens.innerText = '0' + tens
    }
    if ( tens >= 9 ) {
    	appendTens.innerText = tens
    }
    if ( sec <= 9 ) {
    	appendSeconds.innerText = '0' + sec
    }
    if ( tens >= 99 ) {
        sec++
        appendTens.innerText = 00
        appendSeconds.innerText = sec
        tens = 0
    }

}

// start-pause button

startPause.addEventListener('click', changeLabel)

// reset button 

reset.addEventListener('click', () => {

	window.localStorage.clear()
	sec = 0
	tens = 0
	appendSeconds.innerText = '00'
	appendTens.innerText = '00'
	document.querySelector('section').innerText = ''

})

// add button 

add.addEventListener('click', function () {

	const lastTime = JSON.parse(window.localStorage.getItem('last-time'));

	secStr = (sec.toString().length == 1) ? `0${sec}`: sec
    tensStr = (tens.toString().length == 1) ? `0${tens}` : tens
    const currentTime = secStr + ':' + tensStr;
    if(lastTime) {
        window.localStorage.setItem('last-time', JSON.stringify([...lastTime, currentTime]))
    } else {
        window.localStorage.setItem('last-time', JSON.stringify([currentTime]));
    }

    const div = document.createElement('div');
    div.className = 'add_time';
    div.innerText = currentTime
    document.querySelector('section').appendChild(div);

    const button = document.createElement('button');
    button.className = 'add_time_button';
    button.innerText = 'X'
    button.addEventListener('click', function() {
        document.querySelector('section').removeChild(button);
        document.querySelector('section').removeChild(div);
    })
    document.querySelector('section').appendChild(button);

})